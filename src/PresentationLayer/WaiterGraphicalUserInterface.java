package PresentationLayer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import BusinessLayer.RestaurantProcessing;
import DataLayer.TextWriter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Map;
import java.util.Observable;

public class WaiterGraphicalUserInterface  extends Observable implements RestaurantProcessing{
	
	
	public  DefaultTableModel modelW= new DefaultTableModel();
	public  JTable tableW;
	
	static Formatter f;
	DecimalFormat df = new DecimalFormat();
	
	
	public  DefaultTableModel modelOrd= new DefaultTableModel();
	public  JTable tableOrd;
	JTextField tableTxt = new JTextField(3);
	
	final DefaultListModel listModel = new DefaultListModel();  
	//JList listaProd = new JList(listModel);
	static int id=1;
	HashSet<MenuItem> menu ;
	JTextField totalTxt= new JTextField(5);
	
	public WaiterGraphicalUserInterface(HashSet<MenuItem> meniu) {
		JFrame frameW = new JFrame("Waiter Window");
		menu= meniu;
		frameW.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		df.setMaximumFractionDigits(2);
		
		JPanel panelW= new JPanel();
		JPanel panelTotal = new JPanel();
		
		JLabel spaceLbl = new JLabel("    ");
		
		JButton addOrdW = new JButton(" ADD ORDER ");
		addOrdW.setPreferredSize(new Dimension(140,60));
		JButton viewAllW = new JButton(" VIEW ORDERS ");
		viewAllW.setPreferredSize(new Dimension(140,60));
		JButton computeBillW = new JButton(" COMPUTE BILL ");
		computeBillW.setPreferredSize(new Dimension(140,60));
		JButton adminBtn = new JButton(" ADMINISTRATOR ");
		adminBtn.setPreferredSize(new Dimension(180,60));
		JLabel empt1 = new JLabel("  ");
		JLabel totalL= new JLabel("Total price:   ");
		JLabel tableLbl = new JLabel("Table:  ");
		//totalL.setPreferredSize(new Dimension(200,120));
		
		totalTxt.setPreferredSize(new Dimension(500,40));
		JLabel empt2 = new JLabel("  ");
		FlowLayout lay = new FlowLayout();
		
		panelW.setLayout(lay);
		
		modelW.addColumn("Product name");
		modelW.addColumn("Price");
		modelW.addColumn("Items");
		
		
		modelOrd.addColumn("Order ID");
		modelOrd.addColumn("Table");
		modelOrd.addColumn("Items");
		
		
		
		tableW= new JTable(modelW);
		tableOrd = new JTable(modelOrd);
		
		JScrollPane scrollW = new JScrollPane(tableW);
		JScrollPane scrollOrd = new JScrollPane(tableOrd);
		
		tableOrd.getColumnModel().getColumn(0).setPreferredWidth(30);
		tableOrd.getColumnModel().getColumn(1).setPreferredWidth(30);
		tableOrd.getColumnModel().getColumn(2).setPreferredWidth(120);
		
		scrollW.setPreferredSize(new Dimension(500,400));
		scrollOrd.setPreferredSize(new Dimension(400,400));
		
		updateModel();
		
		frameW.setLayout(lay);
		frameW.setSize(1500,550);
		frameW.setLocationRelativeTo(null);
		
		panelW.add(addOrdW);
		panelW.add(empt1);
		panelW.add(computeBillW);
		panelW.add(empt2);
		panelW.add(viewAllW);
		panelW.add(tableLbl);
		panelW.add(tableTxt);
		panelW.add(scrollW);
		panelW.add(spaceLbl);
		panelW.add(scrollOrd);
		
		panelTotal.add(totalL);
		panelTotal.add(totalTxt);
		panelTotal.add(adminBtn);
		frameW.add(panelW);		
		frameW.add(panelTotal);
		
		frameW.setVisible(true);
	
	
		 adminBtn.addActionListener(new ActionListener() {

		  		public void actionPerformed(ActionEvent e) {
		  		    
		  				AdministratorGraphicalUserInterface v= new AdministratorGraphicalUserInterface(Restaurant.getMeniu());
		  				frameW.setVisible(false);
		  		}});
		 
		 

		 addOrdW.addActionListener(new ActionListener() {

		  		public void actionPerformed(ActionEvent e) {
		  			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		  			Date date = new Date();
		  			Order newOrd = new Order(id,date,Integer.parseInt(tableTxt.getText()));
		  			HashSet<MenuItem> itemsBought = new HashSet<MenuItem>(10);	
		  		int[] selectedrows = new int[8];
	                  selectedrows = tableW.getSelectedRows();
               Object obj[] = new Object[5];

	                 for (int i=0; i<selectedrows.length ;i++)
	                {
	                	 BaseProduct newItm = new BaseProduct("",0) ;

	                 Object sel = tableW.getModel().getValueAt(selectedrows[i], 0);
	                 Object sel2 = tableW.getModel().getValueAt(selectedrows[i], 1);
	                 Object sel3 =tableW.getModel().getValueAt(selectedrows[i], 2);
	                
	                 newItm.setName((String)sel);
	                 newItm.setPrice((Float)sel2);
	                   itemsBought.add(newItm);
                    
	                }	
	          createNewOrder(newOrd, itemsBought);
		  	 computeTotal(itemsBought);
		     generateBill(newOrd,itemsBought);
		     updateOrdersTable(Restaurant.getOrders());
		     
		  		}});
		 
		 
			
		 viewAllW.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e)  {
			    	
			    	TextWriter.deserialize();
			    	menu = Restaurant.getMeniu();
			    	updateModel();
			    	updateOrdersTable(Restaurant.getOrders());
					
					}});
			
	}
	

	
	public static void main(String[] args) {
		
		//WaiterGraphicalUserInterface ww = new WaiterGraphicalUserInterface();
		
	}


       public float computeTotal(HashSet<MenuItem> items) {
    	   float priceBill = 0 ;
    	   for(MenuItem itm: items) {
    		   priceBill = priceBill + itm.computePrice();
    	   }
    	   totalTxt.setText(String.valueOf(df.format(priceBill)));
    	   return priceBill;
       }


	@Override
	public void deleteMenuItem(String name ) {
			
	}
	
	@Override
	public
     void generateBill(Order ord, HashSet<MenuItem> items ) {


String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());


    	 //DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    		String fName = "bill"+ ord.getOrderId() +".txt";
    		this.openFile(fName);
    		this.addElement("**********************************\n");
    		this.addElement("*             BILL               *\n");
    		this.addElement("**********************************\n");
    		this.addElement("\n----Table no. : " + ord.getTable() +"----\n");
    		this.addElement("\n----Order time: " + date +"----\n");
    		this.addElement("*       ITEMS PURCHASED       *\n");
    		this.addElement("\n                                 " );
    		for(MenuItem itm: items) {
    		this.addElement("\n----"+ itm.getName() +"............"+ itm.computePrice()+"$----\n");
    		      }
    		this.addElement("\n\n");
    		this.addElement("\n-----------Total : " + df.format(computeTotal(items)) + "$----------");
    		this.closeF();
    		
     }
     
     
 	public void openFile(String st) {
		try {

			f= new Formatter(st);
		}
		catch(Exception ex) {
			System.out.println("Unable to create bill.");
		}
		
	}
	
	public void addElement(String s) {
		f.format("%s",s);
	}
	
	public void closeF() {
	
	f.close();
	}
	


	@Override
	public void editMenuItem(String name, String price) {
		// TODO Auto-generated method stub
		
	}

	void viewAllOrders() {
		modelOrd.setRowCount(0);
		for(MenuItem itm: menu) {
			Object[]  val = new Object[3];
			val[0]= itm.getName();
			val[1]= itm.computePrice();
			val[2]=((CompositeProduct) itm).getIngredients();
			modelOrd.addRow(val);
		}
	
	}

	@Override
	public int computeBillPrice() {
		// TODO Auto-generated method stub
		return 0;
	}
	




	@Override
	public void createMenuItem(MenuItem menuItm) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void createNewOrder(Order ord, HashSet<MenuItem> itemsB) {
	  	 Restaurant.getOrders().put(ord, itemsB);
	  	 id++;
	  	 setChanged();
	  	Restaurant.notifyAllObservers();
		
	}
	
	
	
	void updateModel() {
		//listModel.clear();
		modelW.setRowCount(0);
		if(menu!=null) {
	for(MenuItem itm: menu) {
		Object[] objArr = new Object[10];
		objArr[0]=itm.getName();
	  if(itm instanceof BaseProduct)
		objArr[1]=itm.computePrice();
	  if(itm instanceof CompositeProduct) {
		  objArr[1]=((CompositeProduct)itm).computePrice();
	  }
	if(itm instanceof CompositeProduct) {
		objArr[2]=((CompositeProduct)itm).ingredientsList() ;
	}
		modelW.addRow(objArr);
	    }
   }
}

	 void updateOrdersTable(Map<Order,HashSet<MenuItem>> comenzi) {
		 modelOrd.setRowCount(0);
		 for (Map.Entry<Order, HashSet<MenuItem>> entry : comenzi.entrySet()) {
			    HashSet<MenuItem> items = entry.getValue();
			

			 Object[] obj = new Object[4];
			
			 obj[0] = entry.getKey().getOrderId();
			 obj[1] = entry.getKey().getTable();
			 
			 obj[2] = itemsToString(items);			 
			 
			 modelOrd.addRow(obj);
		 }
		 
	 
 }
	 
	 String itemsToString(HashSet<MenuItem> items){
		 String str="";
		 for(MenuItem itm: items) {
			 str=str.concat("  |  ");
			 str= str.concat(itm.getName());
			 
		 }
		return str; 
	 }



}
