package PresentationLayer;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import BusinessLayer.Restaurant;


public class ChefGraphicalUserInterface implements Observer{
	
	JTextArea txtChf = new JTextArea(100,70);

	JFrame frameC = new JFrame("Chef Window");
	
	public ChefGraphicalUserInterface() {
		
		
		JPanel panelC= new JPanel();
		
		frameC.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameC.setLocationRelativeTo(null);
		
		JButton backBtn = new JButton("BACK");
		backBtn.setPreferredSize(new Dimension(120,60));
		
		JScrollPane paneC = new JScrollPane(txtChf);
		
		//GroupLayout layout = new GroupLayout(panelC);
      // panelC.setLayout(layout);
		
		panelC.add(paneC);
        panelC.add(backBtn);
		
		frameC.setSize(new Dimension(700,550));
		frameC.add(panelC);
		frameC.setVisible(true);
		
		 backBtn.addActionListener(new ActionListener() {

		  		public void actionPerformed(ActionEvent e) {
		  		    
		  				WaiterGraphicalUserInterface v= new WaiterGraphicalUserInterface(Restaurant.getMeniu());
		  				frameC.setVisible(false);
		  		}});
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		//frameC.setVisible(true);
		this.txtChf.append(" New order to be prepared ! Hurry up ! \n \n");
		JOptionPane.showMessageDialog(null, "New order to be prepared ! Hurry up !");
		
	}
	
	public static void main(String[] args) {
		//ChefGraphicalUserInterface.txtChef.append("First line ! ");
		//ChefGraphicalUserInterface cc= new ChefGraphicalUserInterface();
		//WaiterGraphicalUserInterface.getFrameW().setVisible(false);
	}

}
