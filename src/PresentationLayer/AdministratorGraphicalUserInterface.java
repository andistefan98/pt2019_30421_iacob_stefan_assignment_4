package PresentationLayer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import BusinessLayer.RestaurantProcessing;
import DataLayer.TextWriter;
import DataLayer.SerializeXML;


public class AdministratorGraphicalUserInterface implements RestaurantProcessing{
	
	DefaultTableModel modelA= new DefaultTableModel();
	JTable tableA;
	
	  final DefaultListModel listModel = new DefaultListModel();  
	
	JTextField prodNameTxt = new JTextField(15);
	JTextField prodPriceTxt = new JTextField(4);
	static HashSet<MenuItem> menu ;
	
	JList lista = new JList(listModel);
	JComboBox menuBox = new JComboBox();
	

     public AdministratorGraphicalUserInterface(HashSet<MenuItem> meniu) {
	
    	 menu = meniu;
    	 menuBox.setSize(new Dimension(200,80));
    	 
	JFrame frameA = new JFrame("Administrator Window");
	frameA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	
	JPanel panelA= new JPanel();
	JPanel panelFields = new JPanel();
	JPanel panelClose = new JPanel();
	
	
	//tableA.setRowSelectionAllowed(true);
	//tableA.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	
	JButton addItemA = new JButton(" ADD ITEM ");
	addItemA.setPreferredSize(new Dimension(120,60));
	JButton closeBtn = new JButton(" CLOSE ");
	closeBtn.setPreferredSize(new Dimension(120,60));
	JButton viewAllA = new JButton(" VIEW ITEMS ");
	viewAllA.setPreferredSize(new Dimension(120,60));
	JButton deleteItemA = new JButton(" DELETE ITEM ");
	deleteItemA.setPreferredSize(new Dimension(120,60));
	JButton updateItemA = new JButton(" UPDATE ITEM ");
	updateItemA.setPreferredSize(new Dimension(120,60));
	JButton waiterBtn = new JButton(" WAITER ");
	waiterBtn.setPreferredSize(new Dimension(180,60));

	
	JLabel empt1 = new JLabel("  ");
	JLabel empt2 = new JLabel("  ");
	JLabel empt3 = new JLabel("  ");
	//JLabel empt4= new JLabel("                                           ");
	
	JLabel prodName = new JLabel("Product name : ");
	JLabel prodPrice = new JLabel("        Product price : ");
	FlowLayout lay = new FlowLayout();
	
	panelA.setLayout(lay);
	panelFields.setLayout(lay);
	panelClose.setLayout(lay);
	
	modelA.addColumn("Product name");
	modelA.addColumn("Price");
	modelA.addColumn("Items");
	
	tableA= new JTable(modelA);
	tableA.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	
	JScrollPane scrollA = new JScrollPane(tableA);
	
   scrollA.setPreferredSize(new Dimension(750,500));
    DefaultListSelectionModel m = new DefaultListSelectionModel();
    m.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    m.setLeadAnchorNotificationEnabled(true);
    lista.setSelectionModel(m);
	
	
	frameA.setLayout(lay);
	frameA.setSize(1400,700);
	frameA.setLocationRelativeTo(null);
	
	
	panelFields.add(prodName);
	panelFields.add(prodNameTxt);
	panelFields.add(prodPrice);
	panelFields.add(prodPriceTxt);
	
	panelA.add(addItemA);
	panelA.add(empt1);
	panelA.add(deleteItemA);
	panelA.add(empt2);
	panelA.add(updateItemA);
	panelA.add(empt3);
	panelA.add(viewAllA);
	panelA.add(scrollA);
	
	
	//panelA.add(menuBox);
	
	panelClose.add(closeBtn);
	panelClose.add(waiterBtn);
	
	frameA.add(panelFields);
	frameA.add(panelA);
	frameA.add(panelClose);
	
	frameA.setVisible(true);
	
	
	 addItemA.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)  {	    	
		     	
		    	 if(Float.parseFloat(prodPriceTxt.getText())==0) {
		    	
		    		ArrayList<MenuItem> ingredients = new ArrayList<MenuItem>(10);	 
		    		int[] selectedrows = new int[6];
		                  selectedrows = tableA.getSelectedRows();
	                 Object obj[] = new Object[5];

		                 for (int i=0; i<selectedrows.length ;i++)
		                {
		                	 BaseProduct newItm = new BaseProduct("",0) ;

		                 Object sel = tableA.getModel().getValueAt(selectedrows[i], 0);
		                 Object sel2 = tableA.getModel().getValueAt(selectedrows[i], 1);
		                 //Object sel3 =tableA.getModel().getValueAt(i, 2);
		                
		                //String strr = (String) sel2;
		                 newItm.setName((String)sel);
		                 newItm.setPrice((Float)sel2);
		                   ingredients.add(newItm);

		                }		
		             CompositeProduct compus = new CompositeProduct(prodNameTxt.getText(),0,ingredients);
                        //compus.setPrice(compus.computePrice());
		             
		            createMenuItem(compus);
		    			    	
		    	}
		    	
		    	 else if(Float.parseFloat(prodPriceTxt.getText()) >0) {
					createMenuItem(new BaseProduct(prodNameTxt.getText(),Float.parseFloat(prodPriceTxt.getText())));}		    
		   
		    	updateModel();
					
				}});
	 
	 
	 closeBtn.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)  {
		    	SerializeXML.serialize();
		    	
		    	frameA.setVisible(false);
				frameA.dispose();
				}});
	 
	waiterBtn.addActionListener(new ActionListener() {

	  		public void actionPerformed(ActionEvent e) {
	  		    
	  				WaiterGraphicalUserInterface v= new WaiterGraphicalUserInterface(Restaurant.getMeniu());
	  				frameA.setVisible(false);
	  		}});
	 
	 
	 updateItemA.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)  {
		    	
					editMenuItem(prodNameTxt.getText(),prodPriceTxt.getText());
					updateModel();
					
				}});
	 
	 deleteItemA.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)  {
		    	
					deleteMenuItem(prodNameTxt.getText());
					updateModel();
				
				}});      
	 
     viewAllA.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e)  {
		    	
		    	TextWriter.deserialize();
		    	menu = Restaurant.getMeniu();
		    	updateModel();
					//showAll();
				
				}});
         }
    
     public static void main(String[] args) {
    	// HashSet<MenuItem> gol = new HashSet<MenuItem>();
    	 AdministratorGraphicalUserInterface aa= new AdministratorGraphicalUserInterface(Restaurant.getMeniu());
    	 
     }

	@Override
	public void createMenuItem(MenuItem menuItm) {
		if(prodNameTxt.getText()=="" ) {
		}
		else {
		  if(menuItm !=null) {
			
		if(menuItm instanceof CompositeProduct)	{
			
			menuItm.computePrice();
		}
						  
			menu.add(menuItm);
			  
			}
		}
		
		updateModel();
	}

	@Override
	public void deleteMenuItem(String name) {
       MenuItem toBeDeleted=null;
        for(MenuItem menuItm: menu) {
        	try {
        	if(name != null && menu !=null && menuItm!=null) {
        	if(menuItm.getName().equals(name)) {
        		toBeDeleted= menuItm;
        	}
         }
        	}catch(Exception ex) {
        		//JOptionPane.showMessageDialog(null, "Deleting item with provided name .");
        	    ex.getMessage();
        	}
        }
       if(toBeDeleted!=null) {
	  menu.remove(toBeDeleted);
       }
     
    	   
	  updateModel();
	}
	
	
	void updateModel() {
		//listModel.clear();
		modelA.setRowCount(0);
		if(menu!=null) {
	for(MenuItem itm: menu) {
		Object[] objArr = new Object[10];
		objArr[0]=itm.getName();
	  if(itm instanceof BaseProduct)
		objArr[1]=itm.computePrice();
	  if(itm instanceof CompositeProduct) {
		  objArr[1]=((CompositeProduct)itm).computePrice();
	  }
	if(itm instanceof CompositeProduct) {
		objArr[2]=((CompositeProduct)itm).ingredientsList() ;
	}
		modelA.addRow(objArr);
	    }
   }
}

	public void editMenuItem(String name,String price) {
		
		
	 for(MenuItem mm : Restaurant.getMeniu()) {
		 if(mm.getName().equals(name)) {
		     mm.setPrice(Float.parseFloat(price));
		     
	          }
	   }

 }
         

	@Override
	public int computeBillPrice() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void generateBill(Order ord, HashSet<MenuItem> items ) {
		// TODO Auto-generated method stub
		
	}
	
	public void setModel(DefaultTableModel m) {
		modelA= m;
	}

	@Override
	public void createNewOrder(Order ord, HashSet<MenuItem> itemsB) {
		// TODO Auto-generated method stub
		
	}


}