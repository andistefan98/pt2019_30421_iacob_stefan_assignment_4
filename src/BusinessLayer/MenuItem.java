package BusinessLayer;

import java.util.ArrayList;

public interface MenuItem {
     
   float computePrice() ;
   void setPrice(float prc);
   public String getName();
   public void setName(String nm);
   public String ingredientsList();

}
