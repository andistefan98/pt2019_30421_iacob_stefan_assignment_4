package BusinessLayer;

import java.util.HashSet;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import PresentationLayer.AdministratorGraphicalUserInterface;


/*
 * @invariant forall IEmployee e in getEmployees() |
 *              getRooms().contains(e.getOffice())
 */
public class Restaurant extends Observable implements RestaurantProcessing{
	
	static HashSet<MenuItem> meniu = new HashSet<MenuItem>(50);
	private static ArrayList<Observer> observers;
	
	
	HashSet<Order> orderList = new HashSet<Order>();
	static Map<Order,HashSet<MenuItem>> orders = new HashMap<Order,HashSet<MenuItem>>();
	
	
	public Restaurant(HashSet<MenuItem> menu,ArrayList<Observer> obs) {
		observers = obs;
		meniu = menu;
	}
	
	public static void notifyAllObservers() {
		for(Observer obs : observers) {
			obs.update(null, null);
		}
	}
	
	/**
	 * @post forall Order ord in getOrder() | ord.getOrderId() > 0 
	 */
	public static Map<Order,HashSet<MenuItem>> getOrders(){
		return orders;
	}
	
	
	
	public void registerObserver(Observer obs) {
		observers.add(obs);
	}

	/**
	 *  
	 *
	 *  @post meniu.size() = meniu@pre.size() + 1
	 *  @post meniu.contains(menuItm)
	 */
	@Override
	public void createMenuItem(MenuItem menuItm) {
		meniu.add(menuItm);
	}
	
	
	
	@Override
	public void deleteMenuItem(String name) {
		  MenuItem toBeDeleted=null;
	        for(MenuItem menuItm: meniu) {
	        	try {
	        	if(name != null && meniu !=null && menuItm!=null) {
	        	if(menuItm.getName().equals(name)) {
	        		toBeDeleted= menuItm;
	        	}
	         }
	        	}catch(Exception ex) {
	        		//JOptionPane.showMessageDialog(null, "Deleting item with provided name .");
	        	    ex.getMessage();
	        	}
	        }
	       if(toBeDeleted!=null) {
		  meniu.remove(toBeDeleted);
	       }
	     
	}

	
	/**
	 *  
	 *
	 *  @pre price!=null
	 *  @pre name!=null
	 */
	@Override
	public void editMenuItem(String name, String price) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int computeBillPrice() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 *  
	 *
	 *  @pre ord.getOrderId()>0
	 *  @pre ord.getTable()>0
	 */
	@Override
	public void generateBill(Order ord, HashSet<MenuItem> items ) {
		// TODO Auto-generated method stub
		
	}
	
	public static HashSet<MenuItem> getMeniu(){
		return meniu;
	}

	public static void setMeniu(HashSet<MenuItem> mn) {
		meniu = mn ; 
	}
	
	@Override
	public void createNewOrder(Order ord,HashSet<MenuItem> itemsB) {
		orders.put(ord, itemsB);
	}

	public void main(String[] args) {
	  
		//AdministratorGraphicalUserInterface aGUI = new AdministratorGraphicalUserInterface(this.getMeniu());
		
	}
  
}
