package BusinessLayer;

import java.util.HashSet;

public interface RestaurantProcessing {

	public void createMenuItem(MenuItem menuItm);
	
	public void deleteMenuItem(String name);
	
    public void editMenuItem(String name, String price);
    

    public int computeBillPrice();
    
    public void generateBill(Order ord, HashSet<MenuItem> items );

    public void createNewOrder(Order newOrd,HashSet<MenuItem> itemsB);
}
