package BusinessLayer;

import java.util.Date;

import PresentationLayer.ChefGraphicalUserInterface;

public class Order {

	int orderId ;
	Date orderDate ;
	int table;
	

	public Order(int id, Date currDate,int tbl) {
		
		orderId = id;
		currDate = orderDate;
		table = tbl;
	}
	
	public int getOrderId() {
		return orderId;
	}


	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}


	public int getTable() {
		return table;
	}


	public void setTable(int table) {
		this.table = table;
	}

	
	
	
	public Date getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	
	
	@Override
	public int hashCode() {

		return table * orderId *11 % 250;
	}
	
	public void openChefWindow() {
		ChefGraphicalUserInterface chGUI = new ChefGraphicalUserInterface();
	}
	
	

}
