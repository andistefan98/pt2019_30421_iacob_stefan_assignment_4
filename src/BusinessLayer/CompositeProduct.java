package BusinessLayer;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CompositeProduct implements MenuItem{

	String name;
	float price;
	ArrayList<MenuItem> baseProductsL;
	
	public ArrayList<MenuItem> getBaseProductsL() {
		return baseProductsL;
	}

	public void setBaseProductsL(ArrayList<MenuItem> baseProductsL) {
		this.baseProductsL = baseProductsL;
	}

	public CompositeProduct(String nm,float prc ,ArrayList<MenuItem> baseProducts) {
		this.baseProductsL= baseProducts;
		name = nm;
	    price= prc;
	}
	
	public CompositeProduct() {}
	
	@Override
	public float computePrice() {
		float priceF=0;
		for(MenuItem bp : baseProductsL) {
			
		   for(MenuItem itm: Restaurant.getMeniu()) {
			   if(itm.getName().equals(bp.getName())) {
				   bp.setPrice(itm.computePrice());
			   }
		   }
		
		    priceF= priceF + bp.computePrice(); 
		}
	price = priceF;
		return priceF;
		
	}
	
	@Override
	public void setPrice(float prc) {
		price = computePrice();
	}
	

	public float getPrice() {
		return price;
	}
	
	public void addMenuItem(MenuItem menuItm) {
		baseProductsL.add(menuItm);
		
	}
	
	public void deleteMenuItem(MenuItem menuItm) {
		baseProductsL.remove(baseProductsL.indexOf(menuItm));
	}
	
	public void setProductsList(ArrayList<MenuItem> produse) {
		
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String nm) {
		name = nm;
	}
	
	public String getIngredients() {
		String str="";
		 if(baseProductsL!=null) {
		for(MenuItem bpd : baseProductsL) {
			str.concat(bpd.getName() + "  ");
		}
		 }
		return str;
	}
	
	@Override
	public String ingredientsList() {
		String str="   [  ";
		for(MenuItem itm: baseProductsL) {
			str= str.concat(itm.getName());
			str= str.concat("   ");
		}
		str = str.concat("  ] ");
	 
		return str;
	}

	@Override
	public String toString() {
		String str ="";
		DecimalFormat df = new DecimalFormat("#.##");
		return "" + getName() + "" + ingredientsList() +"          " + df.format(price) + " "  ;
			
	}

	
	

}
