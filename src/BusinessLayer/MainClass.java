package BusinessLayer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Observer;

import PresentationLayer.AdministratorGraphicalUserInterface;
import PresentationLayer.ChefGraphicalUserInterface;
import PresentationLayer.WaiterGraphicalUserInterface;

public class MainClass {
    HashSet<MenuItem> meniuFinal = new HashSet<MenuItem>();
    //SerializeXML.serialize();
    ArrayList<Observer> observ = new ArrayList<Observer>();
    static ChefGraphicalUserInterface cGUI = new  ChefGraphicalUserInterface();

	Restaurant marty = new Restaurant(meniuFinal,observ);
    
    WaiterGraphicalUserInterface wGUI ; 
    AdministratorGraphicalUserInterface aGUI ; 
    
	MainClass(){
		observ.add(cGUI);
	    aGUI = new AdministratorGraphicalUserInterface(meniuFinal);
		 
	}
	
	public static void main(String[] args) {
		
		MainClass cls1 = new MainClass();
       // cls1.getRestaurant().addObserver(cls1.getChefView());
	}
	
	public Restaurant getRestaurant() {
		return marty;
	}
	
	public ChefGraphicalUserInterface getChefView() {
		return cGUI;
	}
	
	
}
