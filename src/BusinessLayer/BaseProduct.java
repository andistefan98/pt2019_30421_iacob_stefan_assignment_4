package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class BaseProduct implements MenuItem,Serializable{

	String name ;
	float price;
	
	public BaseProduct(String nm,float prc) {
	   this.name = nm;
	   this.price= prc;
     
	 }
	
	public BaseProduct() {}
	
	
	@Override
	public String toString() {
		//String str="";
		return "" + name + "            " + price + "";
	}

	@Override
  public void setName(String nm) {
	name = nm ;
}
  
  

	@Override
	public float computePrice() {
		return price;
	}
	
	@Override
	public void setPrice(float prc){
		this.price = prc;
	}
	
	

	public float getPrice() {
		return price;
	}



	@Override
	public String getName() {
		return name;
	}

	@Override
	public String ingredientsList() {
		// TODO Auto-generated method stub
		return null;
	}
}
