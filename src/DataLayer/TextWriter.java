package DataLayer;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class TextWriter {

	public TextWriter() {
		
	}
	
	@SuppressWarnings("unchecked")
	public static void deserialize() {
		
		try {
		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("menu2.xml")));
		HashSet<MenuItem> menuItems = new HashSet<MenuItem>();
		menuItems =(HashSet<MenuItem>) decoder.readObject();
		//menuItems.add(itm);
		
		Restaurant.setMeniu(menuItems);
		decoder.close();
	}
		catch(FileNotFoundException ex) {
			ex.getMessage();
		}
}
	
}
